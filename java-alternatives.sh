#!/bin/bash
VERSION="1.0.0"
ROOT=$(ls /home/axlgalicia/.jdks/)
CURRENT=$JAVA_HOME

logotipo(){
    echo "
.-------------------------------------------------------------------------.
|     _   ___     ___                                                     |
|    | | / \ \   / / \                                                    |
| _  | |/ _ \ \ / / _ \                                                   |
|| |_| / ___ \ V / ___ \                                                  |
| \___/_/ _ \_\_/_/ __\_\ ____  _   _    _  _____ _____     _______ ____  |
|   / \  | | |_   _| ____|  _ \| \ | |  / \|_   _|_ _\ \   / / ____/ ___| |
|  / _ \ | |   | | |  _| | |_) |  \| | / _ \ | |  | | \ \ / /|  _| \___ \ |
| / ___ \| |___| | | |___|  _ <| |\  |/ ___ \| |  | |  \ V / | |___ ___) ||
|/_/   \_\_____|_| |_____|_| \_\_| \_/_/   \_\_| |___|  \_/  |_____|____/ |
'-------------------------------------------------------------------------'  version " $VERSION

echo ''

}


bienvenida(){
    echo "Bienvenido, consulte las versiones instaladas de JDK"
}


listJdks(){	
	echo 	"LISTA DE JDKS"
    nList=1
    #Jdks=$(ls /home/axlgalicia/.jdks/)
    echo ''
    Jdks=$ROOT
    for value in $Jdks
    do
        #echo $nList
        echo $nList") "$value
        JdkArray[$nList]=$value
        nList=$(($nList+1))
	done
}

showCurrentVersion(){
    IFS='/'
    read -ra arr <<< "$CURRENT"
    sizeArray=${#arr[@]}
    #echo $sizeArray
    echo "El Jdk actual es: " ${arr[$sizeArray-1]}
    echo ''
    #echo $CURRENT
}

setJdk(){
    echo ''
    read -p ':: Elija el jdk a utilizar > ' uservar
    newJdk=${JdkArray[$uservar]}
    #echo $newJdk
    IFS='/'
    read -ra arr <<< "$CURRENT"
    sizeArray=${#arr[@]}
    currentJdk=${arr[$sizeArray-1]}
    #echo $currentJdk
    echo $(sed -i 's/'$currentJdk'/'$newJdk'/' ~/.bashrc)
    echo ''
    echo 'Listo, ahora tiene que iniciar una nueva sesion de su terminal para ver reflejados los cambios.'
    
}

logotipo



case $1 in
    "-l")
        #echo "quiere la opcion 1"
        listJdks
        ;;
    "-s")
        #showCurrentVersion
        echo "JDKs Disponibles, elija el numero del jdk que utilizara"
        echo ''
        listJdks
        setJdk
        ;;
    "-c")
        echo ''
        showCurrentVersion
    esac